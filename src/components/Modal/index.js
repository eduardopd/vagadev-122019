import React from 'react'
import './styles.css'

export const Modal = ({ 
    handleClose, 
    show, 
    children 
}) => {
    const showHideClassName = show ? "modal display-block" : "modal display-none";
    return(
        <div className={showHideClassName}>
            <section className="App-modal">
                {children}
            </section>
        </div>
    );
};