import React from 'react'
import './styles.css'

export default class AutoComplete extends React.Component {
    constructor (props){
        super(props);
        this.items = [
            'Mario',
            'Mario2',
            'Mario3',
            'Mario4',
            'Mario5',
            'Luigi',
            'Yoshi',
            'Bowser',
            'Peach',
        ];
        this.state = {
            suggestions: [],
            text:'Digite o que procura',
        };
    }

    onTextChanged = (e)=> {
        const value = e.target.value;
        let suggestions = [];
        if (value.length > 0){
            const regex = new RegExp(`^${value}`,'i');
            suggestions = this.items.sort().filter(v => regex.test(v));
        }
        this.setState(() => ({ suggestions, text: value }));
        console.log(this.state.suggestions);
    }

    suggestionSelected (value) {
        this.setState(() => ({
            text: value,
            suggestions: [],
        }))
    }

    renderSuggestions (){
        const { suggestions } = this.state;
        if (suggestions.length === 0){
            return null;
        }
        return (
            <ul>
                {suggestions.map((item) => <li onClick={() => this.suggestionSelected(item)}>{item}</li> )}
            </ul>
        );   
    }

    render (){
        const { text } = this.state;
        return (
            <div className="App-autocomplete">
                <input value={text} onChange={this.onTextChanged}  type="text" />
                    {this.renderSuggestions()}
            </div>
        );
    };
}