import React, { Component } from 'react';
import './App.css';

/* Components Import */ 
import { Button } from './objects/Button/index';
import { Modal } from './components/Modal/index';
import { Backdrop } from './components/Backdrop/index';
import AutoComplete from './components/Autocomplete/index';

/* Images Import */
import logo from './images/logo.svg';
import logo_footer from './images/logo_footer.svg';
import mario from './images/mario.svg';
import luigi from './images/luigi.svg';
import yoshi from './images/yoshi.svg';
import peach from './images/peach.svg';
import bowser from './images/bowser.svg';

/* Icons Import */
import plus_icon from './objects/Icons/plus.svg';
import equal_icon from './objects/Icons/equal.svg';
import arrow_left from './objects/Icons/left-arrow.svg';
import arrow_right from './objects/Icons/right-arrow.svg';
import success_icon from './objects/Icons/success_icon.svg';
import cart from './objects/Icons/shopping-cart.svg';
import search_icon from './objects/Icons/search_icon.svg';


class App extends Component {

  state = {
    show: false,
    cep: false,
    cart: 0,
    cartOpen: false,
    mobilemenu: 0,
  };

  showModal = () => {
    this.setState({ show: true});
    console.log(this.state.show);
  };
  showModalCep = () =>{
    this.setState({ cep: true });
  };
  showModalCart = () =>{
    const cart = document.querySelector('.App-cart-box');
    cart.classList.remove('App-cart-box');
    cart.classList.add('App-cart-box-active');
    /*this.setState({ cartOpen: true });*/
  };
  hideModal = () => {
    this.setState({ show: false})
    console.log(this.state.show);
  };
  hideModalCep = () => {
    this.setState({ cep: false });
  };
  hideModalCart = () => {
    const cart = document.querySelector('.App-cart-box-active');
    cart.classList.remove('App-cart-box-active');
    cart.classList.add('App-cart-box');
    /*this.setState({ cartOpen: false });*/
  };
  addToCart = () => {
    this.setState(prevState => {
      return {cart: prevState.cart + 1}
    })
    console.log(this.state.cart);
  }
  addTwoToCart = () => {
    this.setState(prevState => {
      return {cart: prevState.cart + 2}
    })
    console.log(this.state.cart);
  }
  addToCartShowModal = (event) =>  {
    this.showModal();
    this.addToCart();
  }
  addTwoToCartShowModal = (event) =>  {
    this.showModal();
    this.addTwoToCart();
  }
  menuSlideOpen = () => {
      const menu = document.querySelector('.App-menu-list');
      menu.classList.remove('App-menu-list');
      menu.classList.add('App-menu-list-active');
  }
  menuSlideClose = () => {
      const menu = document.querySelector('.App-menu-list-active');
      menu.classList.remove('App-menu-list-active');
      menu.classList.add('App-menu-list');
  }

  render(){
    return(
      <div className="App">
        <div className="App-container">

          {/* ################################ */}
          {/* Section 1 -> Header - Navigation */}
          {/* ################################ */}

          <section>
            <header className="App-header">
              <nav className="App-nav"> 
                <div className="App-logo"> 
                  <img src={logo} alt="logo" />
                </div>

                <div className="App-menu">
                  <ul className="App-menu-list">
                    <li><a href="#1"> Games </a></li>
                    <li><a href="#2"> Presentes </a></li>
                    <li><a href="#3"> SALE </a></li>
                    <li><div className="App-menu-burguer-button" onClick={this.menuSlideClose}> Fechar </div></li>
                  </ul>
                </div>

                <div className="App-search">
                  <img src={search_icon} className="Search-icon" alt="pesquisar" />
                  <AutoComplete></AutoComplete>
                </div>

                <div className="App-cart">
                  <div className="App-cart-count">
                    <p>{this.state.cart}</p>
                  </div>
                  <img src={cart} className="App-cart-icon" alt="carrinho" onClick={this.showModalCart}/>
                </div>

                {/* Shopping Cart */}
                <div className="App-cart-box">
                  <h2> Carrinho de Compras</h2>
                  <ul>
                    <li><a href="#1"> Produto 1 </a></li>
                    <li><a href="#2"> Produto 2 </a></li>
                    <li><a href="#3"> Produto 3 </a></li>
                    <li><a href="#2"> Produto 4 </a></li>
                    <li><a href="#3"> Produto 5 </a></li>
                  </ul>
                  <Button onClick={this.hideModalCart}
                      type="button"
                      buttonStyle="btn--secondary"
                      buttonSize="btn--medium--modal"
                      >Fechar Carrinho
                  </Button>
                </div>
                

                {/* Responsive Menu */}
                <div className="App-menu-burger" onClick={this.menuSlideOpen}>
                  <div className="line1"></div>
                  <div className="line2"></div>
                  <div className="line3"></div>
                </div>

              </nav>
            </header>
          </section>

          {/* ##################################### */}
          {/* ######  Section 2 -> User Flow  ##### */}
          {/* ##################################### */}
          <section>
            <div className="App-flow">
              <ul>
                <li><a href="#1"> N1 </a> ></li>
                <li><a href="#2"> action figures </a></li>
                <li><a href="#3"> Super Mario </a></li> 
              </ul>
            </div>
          </section>

          {/* ##################################### */}
          {/* #####  Section 3 -> Main Content #### */}
          {/* ##################################### */}
          <section>
            <main className="App-main">

              {/* Section 3.1 - Product Image - Buy */}
              <div className="Product">
                <div className="Product-image"> <img src={mario} alt="imagem produto"/></div>
                <div className="Product-thumb"> <img src={mario} alt="thumbnail produto"/> </div>
                <div className="Product-info">

                  <div className="Product-title">
                  <h1>Action Figure Mario - Coleção topzera das galáxias </h1>
                  </div>

                  <div className="Product-buy"> 
                    <p className="light-grey">de R$ 189,90</p> 
                    <p className="light-grey-inline">por:</p><h2>R$ 149,90</h2>

                    {this.state.show && <Backdrop></Backdrop>}

                    {this.state.show && 
                    <Modal show={this.state.show} handleClose={this.hideModal}>
                      <img src={success_icon} alt="ícone sucesso"/>
                      <h2>PRODUTO ADICIONADO AO CARRINHO</h2> 
                      <Button onClick={this.hideModal}
                      type="button"
                      buttonStyle="btn--secondary"
                      buttonSize="btn--medium--modal"
                      >OK!</Button>
                    </Modal>}

                    {this.state.show !== true &&
                    <Button onClick={this.addToCartShowModal}
                    type="button"
                    buttonStyle="btn--primary"
                    buttonSize="btn--medium"
                    >COMPRA AE</Button>}

                    {this.state.cep && <Backdrop></Backdrop>}

                    {this.state.cep && 
                    <Modal show={this.state.cep} handleClose={this.hideModalCep}>
                      <h2>VALOR DO FRETE É DE R$ 23,90</h2> 
                      <Button onClick={this.hideModalCep}
                      type="button"
                      buttonStyle="btn--secondary"
                      buttonSize="btn--medium--modal"
                      >CONFIRMAR FRETE!</Button>
              
                    </Modal>}

                  </div>

                  <div className="Product-frete"> 
                    <p className="bold-white"> CALCULE O FRETE </p>
                    <div className="cep1">
                      <input type="text" placeholder="00000"></input>  
                    </div>
                    <div className="cep2">
                      <input type="text" placeholder="000"></input>  
                    </div>  
                    <Button onClick={this.showModalCep}
                    type="button"
                    buttonStyle="btn--secondary"
                    buttonSize="btn--small"
                    >calcular</Button>
                  </div>
                </div>
              </div>

              {/* Section 3.2 - Product Description */}
              <div className="Product-desc">
                <h3> Descrição do Produto </h3>
                <p className="light-grey-left-height">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum facere facilis iure corporis et adipisci tempora temporibus ab mollitia esse odit cum! Corrupti et architecto facilis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum facere facilis iure corporis et adipisci tempora temporibus ab mollitia esse odit cum! Corrupti et architecto facilis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum facere facilis iure corporis et adipisci tempora temporibus ab mollitia esse odit cum! Corrupti et architecto facilis. 
                </p>
                <p className="light-grey-left-height">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum facere facilis iure corporis et adipisci tempora temporibus ab mollitia esse odit cum! Corrupti et architecto facilis.
                </p>
              </div>

              {/* Section 3.3 - Product Buy More */}
              <div className="Product-more">
                <div className="More-box-title">
                  <h3> Compre Junto </h3>
                </div>
                <div className="More-box">

                  <div className="More-box1">
                    <img src={mario} alt="imagem produto"/> 
                    <p className="light-grey-left"> Action Figure Mario - Coleção topzera das galáxias </p>
                    <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                    <p className="bold-blue"> por R$ 149,90 </p>
                  </div>
                  <div className="Plus-icon"><img src={plus_icon} alt="ícone mais"/> </div>

                  <div className="More-box2">
                    <img src={yoshi} alt="imagem produto"/> 
                    <p className="light-grey-left"> Action Figure Yoshi - Coleção topzera das galáxias </p>
                    <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                    <p className="bold-blue"> por R$ 100,00 </p>
                  </div>

                  <div className="Equal-icon"><img src={equal_icon} alt="ícone igual"/> </div>
                  <div className="More-box3">
                    <p className="bold-white">Pague somente:</p> 
                    <p className="bold-white-tall">por R$200,00</p> 
                      <div className="More-box-discount">
                        <p className="bold-dark-blue">Economia de: R$ 49,90</p>
                      </div>

                    <Button onClick={this.addTwoToCartShowModal} 
                    type="button"
                    buttonStyle="btn--primary"
                    buttonSize="btn--large"
                    >COMPRA AE</Button>
                  </div>
                </div>
              </div>

              {/* Section 3.4 - Product Related */}
              <div className="Product-related">
                <div className="Related-box-title">
                  <h3>  Quem viu, viu também </h3>
                </div>
                <div className="Related-box"> 
                  <div className="Arrow-icon"><img src={arrow_left} alt="ícone seta"/> </div>

                  <div className="Related-box1">
                    <img src={yoshi} alt="imagem produto"/> 
                    <p className="light-grey-left"> Action Figure Yoshi - Coleção topzera das galáxias </p>
                    <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                    <p className="bold-blue"> por R$ 100,00 </p>
                  </div>

                  <div className="Related-box2">
                    <img src={luigi} alt="imagem produto"/> 
                    <p className="light-grey-left"> Action Figure Luigi - Coleção topzera das galáxias </p>
                    <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                    <p className="bold-blue"> por R$ 149,90 </p>
                  </div>

                  <div className="Related-box3">
                    <img src={peach} alt="imagem produto"/> 
                    <p className="light-grey-left"> Action Figure Peach - Coleção topzera das galáxias </p>
                    <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                    <p className="bold-blue"> por R$ 100,00 </p>
                  </div>

                  <div className="Related-box4">
                    <img src={bowser} alt="imagem produto"/> 
                    <p className="light-grey-left"> Action Figure Yoshi - Coleção topzera das galáxias </p>
                    <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                    <p className="bold-blue"> por R$ 100,00 </p>
                  </div>

                  <div className="Arrow-icon"><img src={arrow_right} alt="ícone seta"/> </div>

                </div>  
              </div>
            </main>
          </section>

          {/* ##################################### */}
          {/* ########  Section 4 -> Footer ####### */}
          {/* ##################################### */}

          <section>
            <footer className="App-footer">
              <div className="App-footer-box">
                <div className="App-footer-left">
                  <div className="App-logo-footer">
                    <img src={logo_footer} alt="logo"/>
                  </div>
                </div>
                <div className="App-copy">
                  <p className="footer"> Agência N1 - Todos os direitos reservados.</p>
                </div>
              </div>
            </footer>
          </section>

        </div>
      </div>
    );
  }
}

export default App;

