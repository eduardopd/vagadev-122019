import { createStore } from 'redux';

const initialState = {
    show: false,
    cep: false,
    cart: 0,
    cartOpen: false,
    mobilemenu: 0,
};

function reducer(state = initialState , action){
    console.log(action);

    if (action.type === 'addToCartShowModal'){
        return { ...state, show: true, cart: action.cart+1};
    }
    if (action.type === 'addTwoToCartShowModal'){
        return { ...state, show: true, cart: action.cart+2};
    }
    if (action.type === 'hideModal'){
        return { ...state, show: false};
    }
    if (action.type === 'showModalCep'){
        return { ...state, cep: true};
    }
    if (action.type === 'hideModalCep'){
        return { ...state, cep: false};
    }

    return state;
}

const store = createStore(reducer);

export default store;

