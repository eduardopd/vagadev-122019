import React, { Component } from 'react';
import AppHeader from './sections/App-header';
import { AppUserflow } from './sections/App-userflow';
import AppMain from './sections/App-main';
import { AppFooter } from './sections/App-footer';
import './Main-App.css';

import { Provider } from 'react-redux';
import store from './store';

class MainApp extends Component {  
    
    render(){
      return(
        <div className="App">
            <div className="App-container">
                <Provider store={store} >
                    <section>
                        <AppHeader  />
                    </section>
                    <section>
                        <AppUserflow />
                    </section>
                    <section>
                        <AppMain />
                    </section>
                    <section>
                        <AppFooter />
                    </section>
                </Provider>
            </div>
        </div> 
      );
    };
}

export default MainApp;