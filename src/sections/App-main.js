import React from 'react'
import './App-main.css'

import { Button } from '../objects/Button/index';
import { Modal } from '../components/Modal/index';
import { Backdrop } from '../components/Backdrop/index';

import mario from '../images/mario.svg';
import luigi from '../images/luigi.svg';
import yoshi from '../images/yoshi.svg';
import peach from '../images/peach.svg';
import bowser from '../images/bowser.svg';

import plus_icon from '../objects/Icons/plus.svg';
import equal_icon from '../objects/Icons/equal.svg';
import arrow_left from '../objects/Icons/left-arrow.svg';
import arrow_right from '../objects/Icons/right-arrow.svg';
import success_icon from '../objects/Icons/success_icon.svg';

import { connect } from 'react-redux';  

    function hideModal(show){
        return {
            type: 'hideModal',
            show: false,
        };
    };

    function addToCartShowModal(show, cart){
        return {
            type: 'addToCartShowModal',
            show: true,
            cart: cart,
        };
    };
    function addTwoToCartShowModal(show, cart){
        return {
            type: 'addTwoToCartShowModal',
            show: true,
            cart: cart,
        };
    };
    function showModalCep(cep){
        return {
            type: 'showModalCep',
            cep: true,
        };
    };
    function hideModalCep(cep){
        return {
            type: 'hideModalCep',
            cep: false,
        };
    };
    const AppMain  = ({ cart, cep, show, dispatch}) => (

            <main className="App-main">

                {/* Section 3.1 - Product Image - Buy */}
                <div className="Product">
                    <div className="Product-image"> <img src={mario} alt="imagem produto"/></div>
                    <div className="Product-thumb"> <img src={mario} alt="thumbnail produto"/> </div>
                    <div className="Product-info">

                        <div className="Product-title">
                            <h1>Action Figure Mario - Coleção topzera das galáxias </h1>
                        </div>

                        <div className="Product-buy"> 
                            <p className="light-grey">de R$ 189,90</p> 
                            <p className="light-grey-inline">por:</p><h2>R$ 149,90</h2>

                            {show && <Backdrop></Backdrop>}

                            {show && 
                            <Modal show={show} handleClose={() => dispatch(hideModal(show))}>
                                <img src={success_icon} alt="ícone sucesso"/>
                                <h2>PRODUTO ADICIONADO AO CARRINHO</h2> 
                                <Button onClick={() => dispatch(hideModal(show))}
                                type="button"
                                buttonStyle="btn--secondary"
                                buttonSize="btn--medium--modal"
                                >OK!</Button>
                            </Modal>}

                            {show !== true &&
                            <Button onClick={() => dispatch(addToCartShowModal(show, cart))}
                            type="button"
                            buttonStyle="btn--primary"
                            buttonSize="btn--medium"
                            >COMPRA AE</Button>}

                            {cep && <Backdrop></Backdrop>}

                            {cep && 
                            <Modal show={cep} handleClose={() => dispatch(hideModalCep(cep))}>
                                <h2>VALOR DO FRETE É DE R$ 23,90</h2> 
                                <Button onClick={() => dispatch(hideModalCep(cep))}
                                type="button"
                                buttonStyle="btn--secondary"
                                buttonSize="btn--medium--modal"
                                >CONFIRMAR FRETE!</Button>
                            </Modal>}
                        </div>

                        <div className="Product-frete"> 
                            <p className="bold-white"> CALCULE O FRETE </p>
                            <div className="cep1">
                                <input type="text" placeholder="00000"></input>  
                            </div>
                            <div className="cep2">
                                <input type="text" placeholder="000"></input>  
                            </div>  
                            <Button onClick={() => dispatch(showModalCep(cep))}
                            type="button"
                            buttonStyle="btn--secondary"
                            buttonSize="btn--small"
                            >calcular</Button>
                        </div>
                    </div>
                </div>

                {/* Section 3.2 - Product Description */}
                <div className="Product-desc">
                    <h3> Descrição do Produto </h3>
                    <p className="light-grey-left-height">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum facere facilis iure corporis et adipisci tempora temporibus ab mollitia esse odit cum! Corrupti et architecto facilis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum facere facilis iure corporis et adipisci tempora temporibus ab mollitia esse odit cum! Corrupti et architecto facilis. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum facere facilis iure corporis et adipisci tempora temporibus ab mollitia esse odit cum! Corrupti et architecto facilis. 
                    </p>
                    <p className="light-grey-left-height">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum asperiores, minus ratione dolorum facere facilis iure corporis et adipisci tempora temporibus ab mollitia esse odit cum! Corrupti et architecto facilis.
                    </p>
                </div>

                {/* Section 3.3 - Product Buy More */}
                <div className="Product-more">
                    <div className="More-box-title">
                        <h3> Compre Junto </h3>
                    </div>
                    <div className="More-box">

                        <div className="More-box1">
                            <img src={mario} alt="imagem produto"/> 
                            <p className="light-grey-left"> Action Figure Mario - Coleção topzera das galáxias </p>
                            <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                            <p className="bold-blue"> por R$ 149,90 </p>
                        </div>
                        <div className="Plus-icon"><img src={plus_icon} alt="ícone mais"/> </div>

                        <div className="More-box2">
                            <img src={yoshi} alt="imagem produto"/> 
                            <p className="light-grey-left"> Action Figure Yoshi - Coleção topzera das galáxias </p>
                            <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                            <p className="bold-blue"> por R$ 100,00 </p>
                        </div>

                        <div className="Equal-icon"><img src={equal_icon} alt="ícone igual"/> </div>

                        <div className="More-box3">
                            <p className="bold-white">Pague somente:</p> 
                            <p className="bold-white-tall">por R$200,00</p> 
                            <div className="More-box-discount">
                                <p className="bold-dark-blue">Economia de: R$ 49,90</p>
                            </div>

                            <Button onClick={() => dispatch(addTwoToCartShowModal(show, cart))} 
                            type="button"
                            buttonStyle="btn--primary"
                            buttonSize="btn--large"
                            >COMPRA AE</Button>
                        </div>
                    </div>
                </div>

                {/* Section 3.4 - Product Related */}
                <div className="Product-related">
                    <div className="Related-box-title">
                        <h3>  Quem viu, viu também </h3>
                    </div>
                    <div className="Related-box"> 
                        <div className="Arrow-icon"><img src={arrow_left} alt="ícone seta"/> </div>

                        <div className="Related-box1">
                            <img src={yoshi} alt="imagem produto"/> 
                            <p className="light-grey-left"> Action Figure Yoshi - Coleção topzera das galáxias </p>
                            <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                            <p className="bold-blue"> por R$ 100,00 </p>
                        </div>

                        <div className="Related-box2">
                            <img src={luigi} alt="imagem produto"/> 
                            <p className="light-grey-left"> Action Figure Luigi - Coleção topzera das galáxias </p>
                            <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                            <p className="bold-blue"> por R$ 149,90 </p>
                        </div>

                        <div className="Related-box3">
                            <img src={peach} alt="imagem produto"/> 
                            <p className="light-grey-left"> Action Figure Peach - Coleção topzera das galáxias </p>
                            <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                            <p className="bold-blue"> por R$ 100,00 </p>
                        </div>

                        <div className="Related-box4">
                            <img src={bowser} alt="imagem produto"/> 
                            <p className="light-grey-left"> Action Figure Yoshi - Coleção topzera das galáxias </p>
                            <p className="light-grey-left-small-height"> de R$ 725,90 </p>
                            <p className="bold-blue"> por R$ 100,00 </p>
                        </div>

                        <div className="Arrow-icon"><img src={arrow_right} alt="ícone seta"/> </div>
                    </div>  
                </div>
            </main>
    );

    export default connect(state => ({ show: state.show, cep: state.cep, cart: state.cart }))(AppMain);