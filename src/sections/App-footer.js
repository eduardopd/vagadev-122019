import React from 'react'
import './App-footer.css'

import logo_footer from '../images/logo_footer.svg';

export const AppFooter = () => {
    return(
        <footer className="App-footer">
            <div className="App-footer-box">
                <div className="App-footer-left">
                    <div className="App-logo-footer">
                        <img src={logo_footer} alt="logo"/>
                    </div>
                </div>
                <div className="App-copy">
                    <p className="footer"> Agência N1 - Todos os direitos reservados.</p>
                </div>
            </div>
        </footer>
    );
};