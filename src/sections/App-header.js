import React from 'react'
import './App-header.css'

import cart_icon from '../objects/Icons/shopping-cart.svg';
import search_icon from '../objects/Icons/search_icon.svg';
import logo from '../images/logo.svg';

import { Button } from '../objects/Button/index';
import AutoComplete from '../components/Autocomplete/index';

import { connect } from 'react-redux';  

    function menuSlideOpen(){
        const menu = document.querySelector('.App-menu-list');
        menu.classList.remove('App-menu-list');
        menu.classList.add('App-menu-list-active');
    };
    function menuSlideClose(){
        const menu = document.querySelector('.App-menu-list-active');
        menu.classList.remove('App-menu-list-active');
        menu.classList.add('App-menu-list');
    };
    function showModalCart(){
        const cart = document.querySelector('.App-cart-box');
        cart.classList.remove('App-cart-box');
        cart.classList.add('App-cart-box-active');
        /*this.setState({ cartOpen: true });*/
    };
    function hideModalCart(){
        const cart = document.querySelector('.App-cart-box-active');
        cart.classList.remove('App-cart-box-active');
        cart.classList.add('App-cart-box');
        /*this.setState({ cartOpen: false });*/
    };

    const AppHeader = ({ cart }) => (
        <header className="App-header">
            <nav className="App-nav"> 
                <div className="App-logo"> 
                    <img src={logo} alt="logo" />
                </div>

                <div className="App-menu">
                    <ul className="App-menu-list">
                        <li><a href="#1"> Games </a></li>
                        <li><a href="#2"> Presentes </a></li>
                        <li><a href="#3"> SALE </a></li>
                        <li><div className="App-menu-burguer-button" onClick={menuSlideClose}> Fechar </div></li>
                    </ul>
                </div>

                <div className="App-search">
                    <img src={search_icon} className="Search-icon" alt="pesquisar" />
                    <AutoComplete></AutoComplete>
                </div>

                <div className="App-cart">
                    <div className="App-cart-count">
                        <p>{cart}</p>
                    </div>
                    <img src={cart_icon} className="App-cart-icon" alt="carrinho" onClick={showModalCart}/>
                </div>

                {/* Shopping Cart */}
                <div className="App-cart-box">
                    <h2> Carrinho de Compras</h2>
                    <ul>
                        <li><a href="#1"> Produto 1 </a></li>
                        <li><a href="#2"> Produto 2 </a></li>
                        <li><a href="#3"> Produto 3 </a></li>
                        <li><a href="#2"> Produto 4 </a></li>
                        <li><a href="#3"> Produto 5 </a></li>
                    </ul>
                    <Button onClick={hideModalCart}
                    type="button"
                    buttonStyle="btn--secondary"
                    buttonSize="btn--medium--modal"
                    >Fechar Carrinho
                    </Button>
                </div>
                    

                {/* Responsive Menu */}
                <div className="App-menu-burger" onClick={menuSlideOpen}>
                    <div className="line1"></div>
                    <div className="line2"></div>
                    <div className="line3"></div>
                </div>
            </nav>
        </header>
    );

    export default connect(state => ({ cart: state.cart }))(AppHeader);


 
